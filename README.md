## PokOwnBot - by the community, for the community.

## Getting Started ##
*in building.*

## Features ##

 - [PTC Login / Google]
 - [Get Map Objects and Inventory]
 - [Search for gyms/pokestops/spawns]
 - [Farm pokestops]
 - [Farm all Pokemon in neighbourhood]
 - [Throw Berries/use best pokeball]
 - [Transfers duplicate pokemons]
 - [Evolve all pokemons]
 - [Throws away unneeded items]
 - [Humanlike Walking]
 - [Configurable Custom Pathing]
 - [Softban bypass]
 - [AutoUpdate / VersionCheck]
 - [Multilanguage Support]
 - [Use lucky egg while evolve]
 - [Egg Hatching Automatically]
 - [Multi bot support]
 - [Snipe pokemon]
 - [Power-Up pokemon]
 - [Telegram Remote Control Support]

## Credits ##

FeroxRev - [RocketAPI](https://github.com/FeroxRev/Pokemon-Go-Rocket-API)

LineWalker - [POGOProtos-0.31.0](https://github.com/Linewalker/POGOProtos-0.31.0)

Valmere - [TransferWeakPokemon](https://github.com/PocketMobsters/PokeMobBot/pull/378/files)

AeonLucid - [POGOProtos](https://github.com/AeonLucid/POGOProtos)


Thanks to everyone who volunteered by contributing via Pull Requests!

## Legal ##

This Website and Project is in no way affiliated with, authorized, maintained, sponsored or endorsed by Niantic, The Pokémon Company, Nintendo or any of its affiliates or subsidiaries. This is an independent and unofficial API for educational use ONLY. 
Using the Project might be against the TOS